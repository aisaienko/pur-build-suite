module.exports = function() {
	'use strict';

	return {
		options: {
			// global options are stored in an external file on the root of the project (.jshintrc)
			jshintrc: '.jshintrc'
		},

		// run jshint for all js files
		all: {
			src: '<%= jscs.all.src %>'
		}
	};
};
