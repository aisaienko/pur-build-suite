module.exports = {
	renderNotes: {
	   	json: '',
	    template: 'build/projects/<%= settings["build.project.name"] %>/notesTemplate.html',
	    dest: 'output/notes-<%= version %>.html'
	}
};