var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    host: 'appmail.ecofabric.com',
    port: 25
}));


module.exports = {

  releaseNotes: { 
    options: {
      transport:transporter,      
	     message: {
        from: 'jenkins-builds@ontrq.com ',
        subject: 'Build Deployment Notification <%= version %>. Release Notes included',
      },
      recipients: [{
        name : "Astound IT Cosmetics",
        email: "itc-all@astoundcommerce.com"
      },
	  {
		  name : "Aaron Weintrob",
		  email: "aaronweintrob@comcast.net"
	  }]
    },
    src: ['output/notes-<%= version %>.html']
  },

  errorNotes: { 
    options: {
      transport:transporter,      
    message: {
        from: 'jenkins-builds@ontrq.com',
        subject: 'Error Notification',
      },
      recipients: [{
        name : "Astound Brandshop",
        email: '<%= instance["recipients"] %>'
      }]
    },
 
    src: ['logs/errorNotes.html']
  }
};