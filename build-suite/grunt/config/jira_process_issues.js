module.exports = {
	options : {
		username: '<%= jira["username"] %>',
		password: '<%= jira["password"] %>',
		host: '<%= jira["host"] %>'
	},
	ci: {
		options: {
			jiraProjectID: "ITC",
			transitionName: "Ready for QA",
			addResolvedToRelease: true,
			readyForDeploymentJQL: "status = Resolved and resolution in (Fixed, Done) and type not in (Risk, Question, Issue)",
			fixVersionJQL: "status!=Closed",
			transitionResolved: true,
			transitionAssignToReporter: false,
			fixVersion: "<%= version %>",
			transitionComment: "Ticket has been deployed to *CI* environment and Ready for Testing. Build Version: <%= version %>",
			releaseJsonFile: 'build/projects/<%= settings["build.project.name"] %>/releases/releaseData-<%= version %>.json'
		}
	}
};