module.exports = {
	releaseNotesTemplate: {
        options: {
        	webResources : {
        		images: false
        	}
        },
        files: {
            'output/notes-<%= version %>.html': 'output/notes-<%= version %>.html'
        }
	}
};