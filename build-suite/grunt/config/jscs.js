module.exports = {
	options: {
		config: '.jscsrc'
	},

	all: {
		src: [
			'Gruntfile.js',
			'build/**/*.js',
			'grunt/**/*.js',
		]
	}
};
