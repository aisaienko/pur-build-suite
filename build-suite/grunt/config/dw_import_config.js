module.exports = function() {
	return {
		options: {
			importTestData: false
		},
		default: {},
		withTestData: {
			options: {
				importTestData: true
			}
		}
	};
};
