'use strict';
/**
 * @module
 * Module containing functions for creating configuration builds.
 */
module.exports = (function(grunt) {
	var importPath;

	/**
	* @private
	*/
	function getVersion() {
		return grunt.config.get('settings.config\\.archive\\.name');
	}

	/**
	 * @private
	 */
	function getImportPath() {
		if (!importPath || !grunt.file.isDir(importPath)) {
			grunt.fail.fatal('Import path is not set or is not a directory');
		}

		return importPath;
	}

	/**
	 * @private
	 *
	 */
	function copyConfiguration(dir) {
		var src = [dir + '/**/*'],
			dest = 'output/site_import/' + getVersion();

		var filter = grunt.config.get('instance.site_import\\.site\\.filter');
		if(filter && typeof filter.map == 'function'){
			src.push('!'+dir + '/sites/**/*');
			src = src.concat(filter.map(function(site){
				return dir + '/sites/' + site+'/**/*';
			}))
			grunt.log.writeln("Found site filter, calculated globbing pattern: "+src);
		}

		grunt.file.expand(src).forEach(function(file) {
			if (grunt.file.isFile(file)) {
				var f = file.replace(dir, '');
				grunt.file.copy(file, dest + f);
			}
		});
	}

	/**
	 *
	 * retrieves files based on replacement file values
	 * @private
	 * @param {Object} replacement - the replacement object
	 */
	function fetchFiles(replacement) {
		// use globbing to modify all files matching given pattern in given rule.
		var files = {};
		replacement.files.forEach(function(file) {
			var allFiles = grunt.file.expand(getBasePath() + '/' + file);
			grunt.log.verbose.writeln('Globbed ' + file + ' to: ', allFiles);

			allFiles.forEach(function(f) {
				files[f] = f;
			});
		});

		return files;
	}

	/**
	 *
	 * @private
	 */
	function getBasePath() {
		return 'output/site_import/' + getVersion() + '/';
	}
	/**
	 *
	 * Modify configuration in the output/site_import folder.
	 * Configures an xmlpoke task to be run after the current executing task.
	 * @private
	 */
	function updateXML(replacements) {
		grunt.log.verbose.writeln('updateXML rules ', replacements);

		// Setup xmlpoke configuration based on the given rules array.

		var files;

		if (typeof replacements !== 'undefined' && replacements.length > 0) {
			replacements.forEach(function(replacement, index) {
				files = fetchFiles(replacement);
				grunt.log.verbose.writeln('Set ' + replacement.xpath + ' to ' + replacement.value, files);
				grunt.config('xmlpoke.' + index, {
					options: {
						xpath: replacement.xpath,
						value: replacement.value
					},
					files: files
				});
			});

			// run the replacements
			grunt.task.run('xmlpoke');
		}
	}

	/**
	 *
	 * update data in xml and text files
	 * @public
	 * @param {Object} replacements - object containing xml and text replacements
	 */
	function updateData(replacements) {
		if (replacements.xmlReplacements) {
			updateXML(replacements.xmlReplacements);
		}

		if (replacements.textReplacements) {
			updateText(replacements.textReplacements);
		}
	}

	/**
	 *
	 * update text replacement data based on grouped files
	 * @private
	 * @param {Array} replacements - Array containing text replacement data
	 */
	function updateText(replacements) {
		var basePath = getBasePath(),
			basePathSep = '|',
			srcString,
			srcFiles = [];

		replacements.forEach(function(replacement, index) {
			srcString = basePath + replacement.files.join(basePathSep + basePath);
			srcFiles = srcString.split(basePathSep);
			grunt.log.verbose.writeln('Set text replace ' + replacement.regex + ' to ' + replacement.value, ' for files', srcFiles);
			grunt.config('replace.' + index, {
				src: srcFiles,
				overwrite: true,
				replacements: [{
					from: replacement.regex,
					to: replacement.value
				}]
			}
		);

			// run the replacements
			grunt.task.run('replace');
		});

	}

	/**
	 * Merge environment and generic configuration into output/site_import folder.
	 */
	function mergeEnvironmentAndGeneric(environment) {
		copyConfiguration(getImportPath() + '/generic');
		copyConfiguration(getImportPath() + '/environment/' + environment);
	}

	/**
	 * Copy additional instance configuration into output/site_import folder over the merged generic and environment configuration.
	 * This allows to override files per instance.
	 * @public
	* @param {String} instance - The instance to target
	 */
	function copyInstanceFiles(instance) {
		copyConfiguration(getImportPath() + '/config/' + instance);
	}

	/**
	 * @public
	 */
	function copyTestDataContent() {
		copyConfiguration(getImportPath() + '/testcontent');
	}

	/**
	* Copy a complete configuration, no need for updating XML and copying extra files.
	 * @public
	 */
	function copyComplete() {
		copyConfiguration(getImportPath());
	}

	/**
	 * Clear given folder in output directory, by default it will clear the site_import folder.
	 * @public
	 * @param {String} directory - Optional directory acting as subfolder for output folder cleanup
	 */
	function cleanup(directory) {
		directory = directory || 'site_import';

		if (grunt.file.isDir('output/' + directory)) {
			grunt.log.verbose.writeln('Cleanup output folder ' + directory);
			grunt.file.delete('output/' + directory);
		}
	}

	/**
 	* @public
 	*/
	function setSourceImportPath(path) {
		grunt.log.verbose.writeln('Site import path is: ', path);
		importPath = path;
	}

	/**
	 * Get the site import file list for given environment.
	 * When no environment is given it expects the generic environment.
	 * @public
	 * @param {String} env - The environment in the demandware repo to target
	 */
	function getSiteImportFileList(env) {
		var siteImportPath = getImportPath();

		if (env) {
			siteImportPath += '/environment/' + env;
		} else {
			siteImportPath += '/generic';
		}

		return grunt.file.expand({cwd: siteImportPath}, '**/*');
	}

	return {
		updateData: updateData,
		copyInstanceFiles: copyInstanceFiles,
		mergeEnvironmentAndGeneric: mergeEnvironmentAndGeneric,
		copyTestDataContent: copyTestDataContent,
		copyComplete: copyComplete,
		cleanup: cleanup,
		setSourceImportPath: setSourceImportPath,
		getSiteImportFileList: getSiteImportFileList
	};
});
