'use strict';
module.exports = function (grunt) {
	grunt.registerMultiTask('dw_logs', 'Download logs', function() {

		var fs = require('fs'),
			request = require('request-promise'),
			Request = require('request'),
			moment = require('moment-timezone'),
			cheerio = require('cheerio'),
			path = require('path'),
			Promise = require('promise'),
			done = this.async(),
			username = grunt.config.get('instance')["webdav.username"],
			password = grunt.config.get('instance')["webdav.password"],
			server = grunt.config.get('instance')["webdav.server"],
			errorType = this.data.errorType,
			date = this.data.date,
			options = {
				uri: server + '/on/demandware.servlet/webdav/Sites/Logs',
				auth: {
					user: username,
					password: password
				}
			};

		request(options)
			.then(getLogsLinks)
			.then(download)
			.then(done);

		function getLogsLinks(body, res, err) {
			if (err) {
				console.error(err);
			}

			var links = [];
			var $ = cheerio.load(body);
			$('td').each(function() {
				var link = $(this).find('a').attr('href');
				if(link!==undefined){
					if(path.extname(link) == '.log'){
						links.push(link);
					}
				}
			});
			return links;
		}

		function  checkDate(date){
			var yesterday = moment().add(-1, 'days').format('YYYYMMDD');
			var today = moment().format('YYYYMMDD');

			if (date ==='today'){
				date = today;
			}else if(date==='yesterday'){
				date = yesterday;
			}else{
				return date
			}
			return date.toString()
		}

		function downloadFile(link) {
			var filename = path.basename(link, '.log');
			var Filename = filename.split('-');
			var errorTypes = errorType.split(', ');
			var filedate = Filename.pop();

			for (var i in errorTypes){
				if(Filename[0]===errorTypes[i] && filedate==checkDate(date)){

				    return new Promise(function(resolve, reject) {
				        try {
				            var stream = fs.createWriteStream('logs/' + filename + '.log');
				            stream.on('finish', function() {
				                grunt.log.writeln(filename + " log downloaded");
				                return resolve(true);
				            });
				            return request({
										url: server+link,
										jar: true,
										auth: {
											user: username,
											password: password
										}
									}).pipe(stream);
				        } catch (e) {
				            return reject(e);
				        }
				    });
				}
			}
		}

		function download(links){
			var calls = [];
			for (var i in links){
				calls.push(downloadFile(links[i]))
			}
			return Promise.all(calls).then(function() {
				grunt.log.writeln("Finished download of all logs.");
			});
		}
	});
}