'use strict';

var lodash = require('lodash');
var grunt = require('grunt');

module.exports = function (grunt) {
  grunt.registerMultiTask('notes_data_preparation', 'Preparation json data for release notes', function() {
    var notes = grunt.file.readJSON(this.data);

    //Transfering email addresses from json to nodemailer task
    function transferMailAdresses(notes){
      var emails = {};

      for (var i in notes.issues){
        var name = notes.issues[i].fields.reporter.displayName;
        var emailAdress = notes.issues[i].fields.reporter.emailAddress;
        var reporter = {email:emailAdress, name:name};
        emails[emailAdress] = reporter;
      }


      grunt.config.merge({
        nodemailer : {
          releaseNotes : {
            options: {
                recipients: lodash.values(emails).concat(grunt.config.get('nodemailer.releaseNotes.options.recipients'))
              }
            }
          }
        });
    }

    function fillGlobals(notes){
      var instance = grunt.config.get('instance');
      notes.globals = {
        buildVersion : grunt.config("settings.build\\.project\\.version"),
        environmentURL: 'https://' + instance["webdav.server"] + "/on/demandware.store/Sites-itcosmetics-us-Site",
      }
    }

    function mustacheDataPreparation(notes){
      var impacted = [];

      //Combine transitioned and added to impacted collection
      for (var i in notes.issues){
        for (var j in notes.addedIssues){
            if(notes.addedIssues[j]==notes.issues[i].key){
              impacted.push(notes.issues[i]);
            }
        }
        for(var k in notes.transitionedIssues){
          if(notes.transitionedIssues[k]==notes.issues[i].key){
            impacted.push(notes.issues[i]);                   
            }
        }
      }

      //Added remaining issues to existing collection
      var issuesKeys = Object.keys(notes.issues);
      var addedKeys = notes.addedIssues;
      var transitionedKeys = notes.transitionedIssues;
      var existingKeys = lodash.difference(issuesKeys, addedKeys, transitionedKeys);

      var existing = [];

      for (var i in notes.issues){
        for (var j in existingKeys){
          if(notes.issues[i].key==existingKeys[j]){
            existing.push(notes.issues[i]); 
          }
        }
      }
        
      //sort by priority and issue type
      var sortImpacted = sort(lodash.uniq(impacted));
      var sortExisting = sort(existing);
      fillGlobals(notes);
      var sorted = {
        impacted: sortImpacted, 
        existing: sortExisting,
        jira: notes
      };

      grunt.config.merge({
        handlebars_render:{
          renderNotes: {
            json: sorted
          }          
        }
      });

    }

    function sort(obj) {
        var sortPriority = lodash.sortBy(obj, function(o) { 
        return o.fields.priority.name;
        });
        var sortIssueType = lodash.sortBy(sortPriority, function(o) { 
        return o.fields.issuetype.name;
        });
        return sortIssueType;
    };

  mustacheDataPreparation(notes);
  transferMailAdresses(notes);

  });
}