'use strict';

var handlebars = require('handlebars');
var fs = require('fs');
var grunt = require('grunt');

module.exports = function (grunt) {
  	grunt.registerMultiTask('handlebars_render', 'Render release notes', function() {
	  	var data =  this.data.json,
	  		template = this.data.template,
	  		dest = this.data.dest;

	  	var handlebarsTemplateSource = fs.readFileSync(template).toString();
        var handlebarsTemplate = handlebars.compile(handlebarsTemplateSource);
        grunt.file.write(dest, handlebarsTemplate(data));
	})
}