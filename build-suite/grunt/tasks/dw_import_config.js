'use strict';

/**
 *	Create site import configuration structure
 **/
module.exports = function(grunt) {
	grunt.registerMultiTask('dw_import_config', 'Create site import package', function() {
		var packageConfig = require('../lib/util/package_config')(grunt),
			instance = grunt.config.get('instance'),
			instanceName = instance['site.import.instance'],
			options = this.options();

		//packageConfig.cleanup();
		packageConfig.setSourceImportPath('output/site_import');

		var instanceConfigFile = 'output/site_import/config/' + instanceName + '/config.json'
		if (instance && grunt.file.exists(instanceConfigFile)) {
			var instanceConfig;

			try {
				instanceConfig = JSON.parse(grunt.file.read(instanceConfigFile));
				grunt.log.verbose.writeln(instanceConfigFile);
				grunt.log.verbose.writeln('Using instance configuration: ', instanceConfig);
			} catch (e) {
				grunt.fail.fatal('No config.json found for instance ' + instanceName);
			}

			packageConfig.mergeEnvironmentAndGeneric(instanceConfig.environment);
			packageConfig.copyInstanceFiles(instanceName);
			if (options.importTestData) {
				grunt.log.verbose.writeln('Packaging test content data');
				packageConfig.copyTestDataContent();
			}

			packageConfig.updateData(instanceConfig.replacements);
		} else {
			packageConfig.copyComplete();
		}
	});
};
