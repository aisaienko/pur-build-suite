'use strict';

module.exports = function (grunt) {
    grunt.registerMultiTask('error_loger', 'Logs', function() {

        var fs = require('fs'),
        path = require('path'),
        dir =  this.data.filesSrc,
        files = fs.readdirSync(dir),
        handlebars = require('handlebars'),
        logsObj = {
            totalErrorsCount: 0
        };

        for (var i in files){
            if (path.extname(files[i]) === '.log') {
                var logText = fs.readFileSync(dir + files[i], "utf8");
                var errorRegExp = /\[(201\d-[01][1-9]-[0-3][1-9]).*?\] ERROR .+?\|\d+?\|(.+?)\|(.+?)\|.*?\d{19} (.+?)(?:\n|\r|\r\n)/gm;
                var customRegExp = /\[(201\d-[01][1-9]-[0-3][1-9]).*?\].+?\|\d+?\|(.+?)\|(.+?)\|.* custom (.+)(?:(?:\[201\d-[01][1-9]-[0-3][1-9].*?\])|$)/gm;
                var warnRegExp = /\[(201\d-[01][1-9]-[0-3][1-9]).*?\] WARN (?:(?:.+?\|\d+?\|(.+?)\|(.+?)\|)|(.+? )).*?\d{19} (.+?)(?:(?:\[201\d-[01][1-9]-[0-3][1-9].*?\])|$)/gm;
                var logElements,
                    baseFileName = path.basename(files[i], '.log');
                if (!baseFileName.indexOf('customerror')) {
                    while (logElements = customRegExp.exec(logText)) {
                        let errorText = logElements[4].trim();
                        addLogToObject(errorText, logElements[2], logElements[3], 'custom');
                    }
                } else if (!baseFileName.indexOf('error')) {
                    while (logElements = errorRegExp.exec(logText)) {
                        let errorText = logElements[4].trim();
                        addLogToObject(errorText, logElements[2], logElements[3], 'error');
                    }
                } else if (!baseFileName.indexOf('warn')) {
                    while (logElements = warnRegExp.exec(logText)) {
                        let errorText = logElements[5].trim();
                        if (logElements[2] && logElements[3]) {
                            addLogToObject(errorText, logElements[2], logElements[3], 'warn');
                        } else {
                            addLogToObject(errorText, logElements[4], logElements[4], 'warn');
                        }

                    }
                }
            }
        }

        var handlebarsTemplateSource = fs.readFileSync('./logs/template.html').toString();
        var handlebarsTemplate = handlebars.compile(handlebarsTemplateSource);
        handlebars.registerHelper("notcounter", function(item) {
            return item !== 'totalErrorsCount';

        });
        grunt.file.write('./logs/errorNotes.html', handlebarsTemplate({
            source: sortLogsByCount(logsObj)
        }));

        function addLogToObject(errorText, siteName, controllerAndAction, type) {
            if (errorText) {
                logsObj.totalErrorsCount++;
                if (!(siteName in logsObj)) {
                    logsObj[siteName] = {
                        totalErrorsCount: 0
                    };
                }
                logsObj[siteName].totalErrorsCount++;
                if (!(controllerAndAction in logsObj[siteName])) {
                    logsObj[siteName][controllerAndAction] = {
                        totalErrorsCount: 0
                    };
                }
                logsObj[siteName][controllerAndAction].totalErrorsCount++;
                if (!(type in logsObj[siteName][controllerAndAction])) {
                    logsObj[siteName][controllerAndAction][type] = {
                        totalErrorsCount: 0
                    };
                }
                logsObj[siteName][controllerAndAction][type].totalErrorsCount++;

                if (!(errorText in logsObj[siteName][controllerAndAction][type])) {
                    logsObj[siteName][controllerAndAction][type][errorText] = {
                        totalErrorsCount: 0
                    };
                }
                logsObj[siteName][controllerAndAction][type][errorText].totalErrorsCount++;
            }
        }

        function sortLogsByCount(logsObj) {
            var objKeys = Object.keys(logsObj);
            objKeys.sort(function(a, b) {
                return logsObj[b].totalErrorsCount - logsObj[a].totalErrorsCount;
            });
            var sortMap = {};
            objKeys.forEach(function(key) {
                if (typeof logsObj[key] === 'object') {
                    sortMap[key] = sortLogsByCount(logsObj[key]);
                } else {
                    sortMap[key] = logsObj[key];
                }
            });
            return sortMap;
        }
    })
}